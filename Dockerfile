#Imagen base
FROM node:latest

#Directorio de la app
WORKDIR /app

#Copia de archivos, añade todo desde donde estoy
ADD /build/default /app/build/default
ADD server.js /app
ADD package.json /app

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3001

#Comando que requiere la app para ejecutar
CMD ["npm", "start"]
